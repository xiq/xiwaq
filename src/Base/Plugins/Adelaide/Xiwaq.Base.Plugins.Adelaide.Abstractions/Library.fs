﻿namespace Xiwaq.Base.Plugins.Adelaide

open Xiwaq.Framework.PluginManagement


type Msg =
    | Increment
    | SwitchAutoIncrement
//    | DoAsyncUpdateTest
    | Decrement
    | Reset

//type Command =
//    | Increment
//    | SwitchAutoIncrement
//    | DoAsyncUpdateTest
//    | Decrement
//    | Reset

//type Event =
//    | Incremented
//    | Decremented
//    | AutoIncrementSwitched
//    | Reset

type State =
    { PluginInfo: PluginInfo
      Count: int
      IsAutoIncrementing: bool }
