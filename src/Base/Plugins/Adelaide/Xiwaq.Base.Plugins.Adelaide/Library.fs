﻿namespace Xiwaq.Base.Plugins.Adelaide

open Reader
open Elmish
open FSharp.Control

open Xiwaq.Framework.PluginManagement


module R = Fable.React.Standard
module R = Fable.React.Helpers
module R = Fable.React.Props



module Base =


    let init (args: InitArgs) =
        { PluginInfo = args.PluginInfo
          Count = 0
          IsAutoIncrementing = false }
        , Cmd.none

    let update msg state =
        match msg with
        | Increment -> { state with Count = state.Count + 1 }, Cmd.none
        | Decrement -> { state with Count = state.Count - 1 }, Cmd.none
        | Reset -> { state with Count = 0 }, Cmd.none
        | SwitchAutoIncrement ->
            let sub dispatch =
                async {
                    while true do
                        do dispatch Increment
                        do! Async.Sleep(2000)
                } |> Async.StartImmediate
            { state with IsAutoIncrementing = not state.IsAutoIncrementing }
            , Cmd.ofSub sub


    let view state dispatch =
        let button name msg =
            R.button [ R.OnClick (fun _ -> dispatch msg) ] [
                R.str name
            ]

        R.div [] [
            R.span [] [
                R.str (sprintf "Count: %i" state.Count)
            ]
            button "Increment" Increment
            button "Decrement" Decrement
            button "Reset" Reset
            button "Switch AutoIncrement" SwitchAutoIncrement
//            button "Test Async Update" Command.DoAsyncUpdateTest
        ]
