module Xiwaq.Base.Plugins.Adelaide.Program

open Xiwaq.Framework.PluginManagement


let init = Base.init
let update = Base.update
let view = Base.view

PluginProgram.mkPlugin init update view
|> PluginProgram.run
