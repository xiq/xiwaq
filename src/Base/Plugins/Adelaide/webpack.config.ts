import * as path from "path"
import * as webpack from "webpack"


function resolve(filePath) {
    return path.isAbsolute(filePath) ? filePath : path.join(__dirname, filePath);
}

type Mode = "development" | "production"

const defineMode = (mode): Mode =>
    mode === "production"
        ? "production"
        : "development"


const config = (env: any, argv: any): webpack.Configuration => {

    const mode = defineMode(argv?.mode)

    return {
        entry: {
            // "485fbe49-f26f-4236-8808-16d463268e10": [
            //     resolve("Xiwaq.Base/dist/Program.js"),
            // ],
            "plugin": [
                resolve("build/Program.js")
            ]
        },
        output: {
            path: resolve("./deploy"),
            filename: "[name].js"
        },
        externals: [
            //'./Framework/Xiwaq.Framework.PluginManagement/Types.js': "dist/Framework/Xiwaq.Framework.PluginManagement/Types",
            // (context, request, callback) => {
            //     console.log("@CONTEXT: ", context);
            //     console.log("@REQUEST: ", request);
            //     callback();
            // }
        ],
        devtool: mode === "production" ? 'source-map' : 'eval-source-map',
        resolve: {
            // See https://github.com/fable-compiler/Fable/issues/1490
            symlinks: false,
            // modules: [
            //     resolve("./node_modules"),
            //     // resolve("./Xiwaq.Base.Abstractions"),
            //     // resolve("../Framework"),
            //     // resolve("../Framework.Fable"),
            // ]
        },
    }
}

export default config;
