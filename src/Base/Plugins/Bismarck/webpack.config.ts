import * as path from "path"
import * as webpack from "webpack"


type Mode = "development" | "production"

const defineMode = (mode): Mode =>
    mode === "production"
        ? "production"
        : "development"


const config = (env: any, argv: any): webpack.Configuration => {

    const mode = defineMode(argv?.mode)

    return {
        entry: {
            "plugin": [
                path.resolve("build/Program.js")
            ]
        },
        output: {
            path: path.resolve("./deploy"),
            filename: "[name].js"
        },
        // @ts-ignore
        externals: {
            //'./Framework/Xiwaq.Framework.PluginManagement/Types.js': "dist/Framework/Xiwaq.Framework.PluginManagement/Types",
            // function(context, request, callback) {
            //     if (request == "./Framework/Xiwaq.Framework.PluginManagement/Types.js") {
            //         return callback(null, "GlobalTestShit")
            //     }
            //     callback();
            // },
            // "./Framework/Xiwaq.Framework.PluginManagement/Types.js": "GlobalTestShit"
        },
        devtool: mode === "production" ? 'source-map' : 'eval-source-map',
        resolve: {
            // See https://github.com/fable-compiler/Fable/issues/1490
            symlinks: false,
            // modules: [
            //     resolve("./node_modules"),
            //     // resolve("./Xiwaq.Base.Abstractions"),
            //     // resolve("../Framework"),
            //     // resolve("../Framework.Fable"),
            // ]
        },
    }
}

export default config;
