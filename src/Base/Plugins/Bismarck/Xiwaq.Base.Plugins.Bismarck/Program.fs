module Xiwaq.Base.Plugins.Bismarck.Program

open Xiwaq.Framework.PluginManagement
open Xiwaq.Base.Plugins.Bismarck.Library


let init = SquaredPlugin.init
let update = SquaredPlugin.update
let view = SquaredPlugin.view

PluginProgram.mkPlugin init update view
|> PluginProgram.run
