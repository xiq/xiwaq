module Xiwaq.Base.Plugins.Bismarck.Library

open Reader
open Xiwaq.Framework.PluginManagement
open Xiwaq.Base.Plugins


type Msg =
    | SwapColor
    | Impostor
    | AdelaideMsg of Adelaide.Msg
    | AdelaideInterfaceGot of PluginInterface<Adelaide.State, Adelaide.Msg>


type ColorState = Red | Black

type State =
    { PluginInfo: PluginInfo
      JustCount: int
      Color: ColorState
      Bridge: IPluginBridge
      AdelaideInterface: PluginInterface<Adelaide.State, Adelaide.Msg> option }

module SquaredPlugin =

    open Elmish
    open FSharp.Control
    open Xiwaq.Framework.PluginManagement


    let init (args: InitArgs) =
        let info = args.PluginInfo
        let sub dispatch =
            async {
                let adelaideInterface = args.InterfaceProvider.GetByName("Adelaide")
                let! adelaideInterface = adelaideInterface
                do dispatch (AdelaideInterfaceGot adelaideInterface)
                do! args.Bridge.Listen adelaideInterface (Msg.AdelaideMsg >> dispatch)
            } |> Async.StartImmediate
        { PluginInfo = info
          JustCount = 0
          Color = Black
          Bridge = args.Bridge
          AdelaideInterface = None }
        , Cmd.ofSub sub

    let update msg state =
        match msg with
        | AdelaideInterfaceGot adelaideInterface ->
            { state with AdelaideInterface = Some adelaideInterface }, Cmd.none
        | SwapColor ->
            match state.Color with
            | Red -> { state with Color = Black }
            | Black -> { state with Color = Red }
            , Cmd.none
        | Impostor ->
            let sub dispatch =
                async {
                    do! state.Bridge.Affect (state.AdelaideInterface |> Option.get) (Adelaide.Increment)
                } |> Async.StartImmediate
            state, Cmd.ofSub sub
        | AdelaideMsg msg ->
            match msg with
            | Adelaide.Increment -> { state with JustCount = state.JustCount + 1 }, Cmd.none
            | Adelaide.Decrement -> { state with JustCount = state.JustCount - 1 }, Cmd.none
            | Adelaide.Reset -> { state with JustCount = 0 }, Cmd.none
            | _ -> state, Cmd.none

    module R = Fable.React.Standard
    module R = Fable.React.Helpers
    module R = Fable.React.Props
    let view state dispatch =
        R.fragment [ ] [
            R.span [
                match state.Color with
                | Red -> R.Style [ R.Color "red" ]
                | Black -> R.Style [ R.Color "black" ]
            ] [
                R.str ^ sprintf "Squared count: %i" (state.JustCount * state.JustCount)
            ]
            R.button [
                R.OnClick (fun _ -> dispatch SwapColor)
            ] [
                R.str "Swap color!"
            ]
            R.button [
                R.OnClick (fun _ -> dispatch Impostor)
            ] [
                R.str "Increment (impostor)"
            ]
        ]
