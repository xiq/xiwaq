[<AutoOpen>]
module Xiwaq.System.PluginManagement.Server.Domain.DomainTypes

open Xiwaq.Framework.PluginManagement


type PluginScript = PluginScript of string

type PluginDomain =
    { Info: PluginInfo
      Script: PluginScript }
