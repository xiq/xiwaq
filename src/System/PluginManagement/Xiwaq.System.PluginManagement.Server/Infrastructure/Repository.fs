module Xiwaq.System.PluginManagement.Server.Infrastructure.Repository

open Reader
open ReaderAsync
open Xiwaq.Framework.PluginManagement

open Xiwaq.System.PluginManagement.Server.Domain
open Xiwaq.System.PluginManagement.Server.Infrastructure.Database


type IHasDataConnection = abstract DataConnection: AppDataConnection

module RepositoryReader =
    let db<'env when 'env :> IHasDataConnection> = Reader.get<'env> <!> fun env -> env.DataConnection


module Queries =

    open Xiwaq.System.PluginManagement.Server.Infrastructure.DaoMappers

    let getScriptByPluginId (PluginId pid) : ReaderAsync<_, _> = reader {
        let! db = RepositoryReader.db
        let q = query {
            for pscript in db.PluginScripts do
            where (pscript.PluginId = pid)
            take 1
        }
        return async {
            let! dao = q |> Query.head
            let script = PluginScript dao.Script
            return script
        }
    }

    let getInfoById (PluginId pid) : ReaderAsync<_, _> = reader {
        let! db = RepositoryReader.db
        let q = query {
            for pinfo in db.PluginInfos do
            where (pinfo.Id = pid)
            take 1
        }
        return async {
            let! dao = q |> Query.head
            let info = dao |> PluginInfo.ofDao
            return info
        }
    }


module Commands =
    do ()
