namespace Xiwaq.System.PluginManagement.Server

open Xiwaq.System.PluginManagement.Server.Infrastructure
open Xiwaq.System.PluginManagement.Server.Infrastructure.Repository


type AppEnv =
    { DataConnection: AppDataConnection }
    interface IHasDataConnection with member this.DataConnection = this.DataConnection


module AppEnv =

    open System
    open Microsoft.Extensions.DependencyInjection

    let factory (sp: IServiceProvider) =
        { DataConnection = sp.GetRequiredService<_>() }
