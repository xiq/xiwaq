module Xiwaq.System.PluginManagement.Server.Library

open System.IO
open Reader
open Xiwaq.Framework.PluginManagement
open Xiwaq.System.PluginManagement.Server.Domain
open Xiwaq.System.PluginManagement.Server.Infrastructure


type IPluginScriptProvider =
    abstract GetScript: PluginId -> Async<PluginScript>


type PluginScriptProvider(appenv: AppEnv) =

    interface IPluginScriptProvider with
        member _.GetScript(pluginId) =
            Repository.Queries.getScriptByPluginId pluginId |> Reader.run <| appenv

type HardPluginScriptProvider() =

    let pluginsPath = "../../../Base/Plugins/"

    interface IPluginScriptProvider with
        member _.GetScript(pluginId) = async {
            let (PluginId pid) = pluginId
            let scriptPath =
                match string pid with
                | "485fbe49-f26f-4236-8808-16d463268e10" -> sprintf "%s/%s" pluginsPath "Adelaide/deploy/plugin.js"
                | "f301a6a7-be04-4140-bc21-4c9970998b68" -> sprintf "%s/%s" pluginsPath "Bismarck/deploy/plugin.js"
                | _ -> failwith "Couldn't hard plugin!"
            let! script = File.ReadAllTextAsync(scriptPath) |> Async.AwaitTask
            return PluginScript script
        }


module PluginManager =

    let getInfoById (pluginId: PluginId) = reader {
        return! Repository.Queries.getInfoById pluginId
    }


type PluginManager(appenv: AppEnv) =
    interface IPluginManager with
        member this.GetAllPluginInfos() = notImplemented ""
        member this.GetInfoById(pluginId) = PluginManager.getInfoById pluginId |> Reader.run <| appenv
