module Xiwaq.System.PluginManagement.Server.HttpHandlers

open System
open Giraffe
open FSharp.Control.Tasks.V2
open Reader

open Xiwaq.Framework.PluginManagement
open Xiwaq.Framework.PluginManagement.Api
open Xiwaq.Framework.PluginManagement.Api.DtoMappers
open Xiwaq.Framework.PluginManagement.Api.PluginManager
open Xiwaq.Framework.PluginManagement.Wrappers
open Xiwaq.System.PluginManagement.Server.Domain
open Xiwaq.System.PluginManagement.Server.Infrastructure
open Xiwaq.System.PluginManagement.Server.Library


let getScript pid : HttpHandler =
    fun next ctx -> task {
        let pluginScriptProvider = ctx.GetService<IPluginScriptProvider>()
        let pluginId = PluginId.Unchecked.create pid
        let! pluginScript = pluginScriptProvider.GetScript(pluginId)
        let pscript = pluginScript |> fun (PluginScript x) -> x

        return!
            setHttpHeader "Content-Type" "application/javascript"
            >=> setBodyFromString pscript
            <|| (next, ctx)
    }


let getInfo: HttpHandler =
    fun next ctx -> task {
        let pluginManager = ctx.GetService<IPluginManager>()
        let! req = ctx.BindModelAsync<GetInfoById.Request>()
        let pluginId = req.PluginId |> PluginId.Unchecked.create
        let! pluginInfo = pluginManager.GetInfoById(pluginId)
        let resp =
            { GetInfoById.Response.PluginInfo = pluginInfo |> PluginInfo.toDto }
        return!
            negotiate resp
            <|| (next, ctx)
    }


[<CLIMutable>]
type GetScriptRouteArg =
    { Id: Guid }


let webapp: HttpHandler =
    choose [
        GET >=> routeBind<GetScriptRouteArg> PluginScriptLinkProvider.GetLink.route (fun arg -> getScript arg.Id)
        POST >=> route GetInfoById.route >=> getInfo
        RequestErrors.NOT_FOUND "Not found"
    ]
