﻿import * as path from "path"
import * as webpack from "webpack";
import * as HtmlWebpackPlugin from "html-webpack-plugin";
import * as CopyWebpackPlugin from "copy-webpack-plugin";
import * as MiniCssExtractPlugin from "mini-css-extract-plugin";
// import DotenvPlugin from "dotenv-webpack";


function resolve(filePath) {
    return path.isAbsolute(filePath) ? filePath : path.join(__dirname, filePath);
}


let CONFIG = {
    indexHtmlTemplate: './Xiwaq.System.WebApp/public/index.html',
    fsharpEntry: './Xiwaq.System.WebApp/Xiwaq.System.WebApp.fsproj',
    cssEntry: './Xiwaq.System.WebApp/style.scss',
    outputDir: './deploy',
    assetsDir: './Xiwaq.System.WebApp/public',
    devServerPort: 8080,
    // When using webpack-dev-server, you may need to redirect some calls
    // to a external API server. See https://webpack.js.org/configuration/dev-server/#devserver-proxy
    devServerProxy: {
        // redirect requests that start with /api/ to the server on port 8085
        '/api/**': {
            target: 'http://localhost:' + (process.env.SERVER_PROXY_PORT || "8085"),
            changeOrigin: true
        },
        // redirect websocket requests that start with /socket/ to the server on the port 8085
        '/socket/**': {
            target: 'http://localhost:' + (process.env.SERVER_PROXY_PORT || "8085"),
            ws: true
        },
        // '/**': {
        //     target: 'http://localhost:5000',
        //     changeOrigin: true
        // }
    },
}

// If we're running the webpack-dev-server, assume we're in development mode
const isProduction = !process.argv.find(v => v.indexOf('webpack-dev-server') !== -1);
console.log('Bundling for ' + (isProduction ? 'production' : 'development') + '...');

// The HtmlWebpackPlugin allows us to use a template for the index.html page
// and automatically injects <script> or <link> tags for generated bundles.
const commonPlugins = [
    new HtmlWebpackPlugin({
        filename: 'index.html',
        template: resolve(CONFIG.indexHtmlTemplate)
    })
];


const config = (env, argv): webpack.Configuration => {

    const mode = argv.mode ?? "development"
    const isDevelopment = mode === "development"
    const isProduction = mode === "production"

    console.log("mode: " + mode)

    return {
        entry:
            // isProduction ? {
            //     app: [resolve(CONFIG.fsharpEntry), resolve(CONFIG.cssEntry)]
            // } : {
            //     app: [resolve(CONFIG.fsharpEntry)],
            //     style: [resolve(CONFIG.cssEntry)]
            // },
            {
                wepapp: resolve("build/Program.js"),
                // "GlobalTestShit": resolve("build/Framework/Xiwaq.Framework.PluginManagement/Types.js")
            },
        output: {
            path: resolve(CONFIG.outputDir),
            filename: isProduction ? '[name].[hash].js' : '[name].js',
            publicPath: "/"
        },
        mode: isProduction ? 'production' : 'development',
        devtool: isProduction ? 'source-map' : 'eval-source-map',
        optimization: {
            splitChunks: {
                chunks: 'all'
            },
        },
        // Besides the HtmlPlugin, we use the following plugins:
        // PRODUCTION
        //      - MiniCssExtractPlugin: Extracts CSS from bundle to a different file
        //          To minify CSS, see https://github.com/webpack-contrib/mini-css-extract-plugin#minimizing-for-production
        //      - CopyWebpackPlugin: Copies static assets to output directory
        // DEVELOPMENT
        //      - HotModuleReplacementPlugin: Enables hot reloading when code changes without refreshing
        plugins: isProduction ?
            commonPlugins.concat([
                new MiniCssExtractPlugin({ filename: 'style.[hash].css' }),
                new CopyWebpackPlugin([{ from: resolve(CONFIG.assetsDir) }]),
            ])
            : commonPlugins.concat([
                new webpack.HotModuleReplacementPlugin(),
            ]),
        resolve: {
            // See https://github.com/fable-compiler/Fable/issues/1490
            symlinks: false,
            // modules: [
            //     resolve("./node_modules/"),
            //     resolve("../../Framework/"),
            //     resolve("../../Framework/Xiwaq.Framework.Utils/"),
            //     resolve("../../Framework.Fable/"),
            // ]
        },
        // Configuration for webpack-dev-server
        devServer: {
            publicPath: '/',
            contentBase: resolve(CONFIG.assetsDir),
            host: '0.0.0.0',
            port: CONFIG.devServerPort,
            proxy: CONFIG.devServerProxy,
            hot: true,
            inline: true,
            historyApiFallback: true
        },
        module: {
            rules: [
                // sass-loaders: transforms SASS/SCSS into JS
                {
                    test: /\.(sass|scss|css)$/,
                    use: [
                        isProduction
                            ? MiniCssExtractPlugin.loader
                            : 'style-loader',
                        'css-loader',
                        {
                            loader: 'resolve-url-loader',
                        },
                        {
                            loader: 'sass-loader',
                            options: { implementation: require('sass') }
                        }
                    ],
                },
                // file-loader: Moves files referenced in the code (fonts, images) into output folder
                {
                    test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)(\?.*)?$/,
                    use: ['file-loader']
                }
            ]
        }
    }
};

export default config;
