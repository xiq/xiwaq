[<AutoOpen>]
module Browser.Dom

open System.Collections
open System.Collections.Generic
open Fable.Core


[<AllowNullLiteral>]
type URLSearchParams private (``_``: Void) =
    [<Emit "new globalThis.URLSearchParams($0)">]
    new (s: string) = URLSearchParams(jsNative<Void>)

    [<Emit "$0.append($1, $2)">]
    member _.append(q: string, v: string): unit = jsNative

    [<Emit "$0.delete($1)">]
    member _.delete(q: string): unit = jsNative

    [<Emit "$0.has($1)">]
    member _.has(q: string): bool = jsNative

    [<Emit "$0.get($1)">]
    member _.get(q: string): string option = jsNative

    [<Emit "$0.getAll($1)">]
    member _.getAll(q: string): string array = jsNative

    [<Emit "$0.entries()">]
    member _.entries(): (string * string) seq = jsNative

    [<Emit "$0.keys()">]
    member _.keys(): string seq = jsNative

    [<Emit "$0.values()">]
    member _.values(): string seq = jsNative

    interface IEnumerable<string * string> with
        member this.GetEnumerator() = this.entries().GetEnumerator()
        member this.GetEnumerator(): IEnumerator = upcast (this :> IEnumerable<_>).GetEnumerator()
