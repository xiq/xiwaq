module Xiwaq.System.WebApp.Authenticator

open Elmish
open Feliz.Router

open Xiwaq.Framework.UserManagement


type RawUrl = string list

[<AutoOpen>]
module private Helpers =

    module InputToken =

        open Xiwaq.System.WepApp.Authentication

        let tryGetFromUrl (url: RawUrl) =
            match url with
            | [ "auth"; Route.Query [ "token", rtoken' ] ] ->
                Ok ^ InputToken rtoken'
            | _ -> Error ()

        let tryGetFromStorage (storage: Browser.Types.Storage) =
            let r = storage.getItem("x-token")
            match r with
            | null -> Error ()
            | s -> Ok ^ InputToken s

        let tryGetFromUrlOrStorage url storage =
            tryGetFromUrl url
            |> Result.bindError (fun () -> tryGetFromStorage storage)

        let toMe (inputToken: InputToken) = async {
            let! r = Auth.getMe inputToken
            return r
        }


type FailureReason =
    | CannotReceiveInputToken
    | CannotReceiveMe

type State =
    | InProgress
    | Failed of FailureReason
    | Resolved of Me

type Msg =
    | InputTokenReceived of Result<InputToken, unit>
    | MeReceived of Result<Me, GetMeError>


let init () =
    let sub dispatch =
        let url = Router.currentUrl()
        let storage = Browser.Dom.window.localStorage
        let r = InputToken.tryGetFromUrlOrStorage url storage
        match r with
        | Ok inputToken -> dispatch ^ Msg.InputTokenReceived ^ Ok inputToken
        | Error () -> dispatch ^ Msg.InputTokenReceived ^ Error ()
    State.InProgress, Cmd.ofSub sub

let update msg state =
    match msg with
    | InputTokenReceived r ->
        match r with
        | Error () -> State.Failed CannotReceiveInputToken, Cmd.none
        | Ok inputToken ->
            let ofSuccess = function Ok me -> Msg.MeReceived ^ Ok me | Error err -> Msg.MeReceived ^ Error err
            state, Cmd.OfAsync.perform InputToken.toMe inputToken ofSuccess
    | MeReceived r ->
        match r with
        | Error _ -> State.Failed CannotReceiveMe, Cmd.none
        | Ok me -> State.Resolved me, Cmd.none


//module React =
//
//    open Feliz
//    open Feliz.ElmishComponents
//
//    type ElmishFunctions<'state, 'msg> =
//        { init: Me -> 'state * Cmd<'msg>
//          update: 'msg -> 'state -> 'state * Cmd<'msg>
//          view: 'state -> Dispatch<'msg> -> ReactElement }
//
//    type StateRenderers =
//        { OnFailed: unit -> ReactElement
//          OnInProgress: unit -> ReactElement }
//
//    [<RequireQualifiedAccess>]
//    type State =
//        | InProgress
//        | Failed
//        | MeResolved of Me
//
//    let render () = React.functionComponent("Authenticator", fun (prop: {| renderers: StateRenderers; elmish: ElmishFunctions<'state, 'msg> |}) ->
//        let state, setState = React.useState(State.InProgress)
//        React.useEffectOnce(fun () ->
//            let url = Router.currentUrl()
//            let storage = Browser.Dom.window.localStorage
//            let r = RawToken.tryGetFromUrlOrStorage url storage
//            match r with
//            | Ok rtoken ->
//                let r = RawToken.toMe rtoken |> Async.RunSynchronously
//                match r with
//                | Ok me -> setState ^ State.MeResolved me
//                | _ -> setState State.Failed
//            | _ -> setState State.Failed
//        )
//
//        match state with
//        | State.InProgress -> prop.renderers.OnInProgress ()
//        | State.Failed -> prop.renderers.OnFailed ()
//        | State.MeResolved me ->
//            let elmish = prop.elmish
//            React.elmishComponent("", elmish.init me, elmish.update, elmish.view)
//    )

