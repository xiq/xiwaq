﻿module Xiwaq.System.WebApp.Program

open Feliz
open Reader
open Xiwaq.System.WebApp.App
open Xiwaq.System.WebApp.Library
open Xiwaq.System.WebApp.Services

// Remove loading-label
Browser.Dom.document.querySelector("#loading-label").remove()


open Fable.Core
open Fable.Core.JsInterop


//let _GlobalTestShit: obj = import "*" "/home/prunkles/Develop/repos/xiq/Xiwaq/src/System/WebApp/build/Framework/Xiwaq.Framework.PluginManagement/Types.js" //"Framework/Xiwaq.Framework.PluginManagement/Types.js"
//
//[<Emit """
//globalThis.GlobalTestShit = _GlobalTestShit;
//console.log("GlobalTestShit:", GlobalTestShit)
//""">]
//let foo _ : unit = jsNative
//foo ()


open System
open Elmish
open Elmish.React
open Xiwaq.Framework.PluginManagement
open Xiwaq.Framework.UserManagement


module Impl =
    open Xiwaq.Framework.UserManagement.Client.Fable
    open Xiwaq.Framework.UserManagement.Wrappers
    open Xiwaq.Framework.PluginManagement.Client

    let tokenProvider =
        { new ITokenProvider with member _.Token = Token.Unchecked.create "" }

    let pluginMsgHost = Uri "http://localhost:5003"

    let pluginManager: IPluginManager = upcast FetchPluginManager(tokenProvider, pluginMsgHost)

    let pluginScriptLinkProvider: IPluginScriptLinkProvider = upcast PluginScriptLinkProvider(pluginMsgHost)

    let pluginLoader: IPluginLoader = upcast PluginLoader(pluginManager, pluginScriptLinkProvider)

    let userMngHost = Uri Unchecked.defaultof<_>

    let authenticationService = FetchAuthenticationService(userMngHost)

let appenv =
    { PluginLoader' = Impl.pluginLoader
      AuthenticationService' = Impl.authenticationService }

module Tmp =

    let loadPlugin (pluginLoader: IPluginLoader) guid =
        async {
            let pluginId = PluginId ^ Guid(guid: string)
            let! plugin = pluginLoader.Load(pluginId)
            return plugin
        }

    let loadAllPlugins () = reader {
        let! pluginLoader = PluginLoader.get
        return async {
            let! p1 = loadPlugin pluginLoader "485fbe49-f26f-4236-8808-16d463268e10"
            let! p2 = loadPlugin pluginLoader "f301a6a7-be04-4140-bc21-4c9970998b68"
            return seq [ p1; p2; ]
        }
    }

let plugins =
    async {
        let! plugins = Tmp.loadAllPlugins () |> Reader.run <| appenv
        return plugins
    }

async {
    let! plugins = plugins
    let element = app {| plugins = plugins |}
    do ReactDOM.render(element, Browser.Dom.document.getElementById("app"))
}
|> Async.StartImmediate
