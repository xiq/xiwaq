module Xiwaq.System.WebApp.App

open Fable.Core
open Reader
open FSharp.Control
open Feliz
open Feliz.UseElmish
open Elmish
open Xiwaq.Framework.PluginManagement

let StreamCompletedWarn () = Browser.Dom.console.warn("Unreachable stream completion!")
let StreamCompletedWarnAsync = async { do StreamCompletedWarn () }

let _LOG fmt = printfn fmt


module Async =
    let blockAwait (a: Async<'a>) =
        let mutable result = Unchecked.defaultof<_>
        async {
            let! r = a
            result <- r
        } |> Async.StartImmediate
        result


type PluginComponentProps =
    { Plugin: Plugin
      Bridge: IPluginBridge
      Publish: Async<IAsyncObserver<DynamicMsg>>
      Subscribe: Async<IAsyncObservable<DynamicMsg>>
      InterfaceProvider: IPluginInterfaceProvider
      }

let pluginComp = React.functionComponent("Plugin", fun (input: PluginComponentProps) ->

    let { Program = program; Info = info } = input.Plugin

    let publish msg =
        async {
            let! msgObv = input.Publish
            do! msgObv.OnNextAsync(msg)
        } |> Async.StartImmediate

    let subscribe dispatch =
        let unsub = async {
            let! msgs = input.Subscribe
            let! unsub =
                msgs.SubscribeAsync(function
                    | OnNext msg -> async { dispatch msg }
                    | OnError err -> async { JS.console.error(sprintf "%A raised error:\n%A" info.Name err); raise err }
                    | OnCompleted -> async { JS.console.warn(sprintf "%A completed msg stream" info.Name) }
                )
            return unsub
        }
        unsub |> Async.blockAwait |> fun x -> x.ToDisposable()

    let state, dispatch =
        let init () =
            let args =
                { PluginInfo = info
                  Bridge = input.Bridge
                  InterfaceProvider = input.InterfaceProvider }
            // Втиснуться в пайплайн init-а, чтобы получать все его dispatch-и
            let state, cmd = program.init args
            let cmd =
                cmd
                |> List.map ^fun sub ->
                    let sub dispatch =
                        let dispatch msg =
                           publish msg
                           dispatch msg
                        sub dispatch
                    sub
            state, cmd

        // Втиснуться в пайплайн update-а, чтобы получать все его dispatch-и
        let update msg state =
            let state, cmd = program.update msg state
            let cmd =
                cmd
                |> List.map ^fun sub ->
                    let sub dispatch =
                        let dispatch msg =
                            publish msg
                            dispatch msg
                        sub dispatch
                    sub
            state, cmd

        React.useElmish(init, update)


    React.useEffect(fun () ->
        subscribe ^fun msg ->
            dispatch msg
            publish msg
    , [| |])

    let dispatch' msg =
        publish msg
        dispatch msg

    program.view state dispatch'
)

type PluginHubMsg =
    | Listen of targetId: PluginId * AsyncReplyChannel<IAsyncObservable<DynamicMsg>>
    | Affect of targetId: PluginId * AsyncReplyChannel<IAsyncObserver<DynamicMsg>>

    | Subscribe of thisId: PluginId * AsyncReplyChannel<IAsyncObservable<DynamicMsg>>
    | Publish   of thisId: PluginId * AsyncReplyChannel<IAsyncObserver<DynamicMsg>>


type PluginHub() =

    let agent = MailboxProcessor.Start(fun inbox ->
//        let msgObv, msgs = AsyncRx.subject<PluginDynamicMsg> ()
        let affectMsgObv, affectMsgs = AsyncRx.subject<PluginDynamicMsg> ()
        let listenMsgObv, listenMsgs = AsyncRx.subject<PluginDynamicMsg> ()
        let rec loop () = async {
            let! msg = inbox.Receive()
            match msg with
            | Listen (targetId, channel) ->
                let targetMsgs =
                    listenMsgs
                    |> AsyncRx.choose ^fun (id, msg) -> if id = targetId then Some msg else None
                channel.Reply(targetMsgs)
                return! loop ()
            | Affect (targetId, channel) ->
                let targetMsgObv =
                    AsyncObserver.Create(function
                        | OnNext msg -> affectMsgObv.OnNextAsync(targetId, msg)
                        | _ -> StreamCompletedWarnAsync
                    )
                channel.Reply(targetMsgObv)
                return! loop ()

            | Subscribe (thisId, channel) ->
                let thisMsgs =
                    affectMsgs
                    |> AsyncRx.choose ^fun (id, msg) -> if id = thisId then Some msg else None
                channel.Reply(thisMsgs)
                return! loop ()
            | Publish (thisId, channel) ->
                let thisMsgObv =
                    AsyncObserver.Create(function
                        | OnNext msg -> listenMsgObv.OnNextAsync(thisId, msg)
                        | _ -> StreamCompletedWarnAsync
                    )
                channel.Reply(thisMsgObv)
                return! loop ()
        }
        loop ()
    )

    member _.Subscribe(thisId: PluginId): Async<IAsyncObservable<DynamicMsg>> =
        agent.PostAndAsyncReply(fun ch -> Subscribe (thisId, ch))
    member _.Publish(thisId: PluginId): Async<IAsyncObserver<DynamicMsg>> =
        agent.PostAndAsyncReply(fun ch -> Publish (thisId, ch))
    member _.Listen(targetId: PluginId): Async<IAsyncObservable<DynamicMsg>> =
        agent.PostAndAsyncReply(fun ch -> Listen (targetId, ch))
    member _.Affect(targetId: PluginId): Async<IAsyncObserver<DynamicMsg>> =
        agent.PostAndAsyncReply(fun ch -> Affect (targetId, ch))


let app = React.functionComponent("Xiwaq.App", fun (input: {| plugins: Plugin seq |}) ->

    let hub = PluginHub()

    let bridge =
        { new IPluginBridge with
            member _.Listen target dispatch = async {
                let! targetMsgs = hub.Listen(target.Info.Id)
                let! unsub =
                    targetMsgs.SubscribeAsync(function
                        | OnNext targetMsg -> async { do dispatch (unbox targetMsg) }
                        | _ -> StreamCompletedWarnAsync
                    )
                ()
            }
            member _.Affect target targetMsg = async {
                let! targetMsgObv = hub.Affect(target.Info.Id)
                do! targetMsgObv.OnNextAsync(unbox targetMsg)
                ()
            }
        }

    let interfaceProvider =
        { new IPluginInterfaceProvider with
            member _.GetByName(name) = async {
                let pluginInterface =
                    input.plugins
                    |> Seq.find ^fun p ->
                        let (PluginName name') = p.Info.Name
                        name = name'
                    |> unbox
                return pluginInterface
            }
        }

    let pluginElements =
        input.plugins
        |> Seq.map ^fun plugin ->
            let info = plugin.Info
            let input =
                { Plugin = plugin
                  Bridge = bridge
                  Publish = hub.Publish(info.Id)
                  Subscribe = hub.Subscribe(info.Id)
                  InterfaceProvider = interfaceProvider }
            pluginComp input

    pluginElements
)
