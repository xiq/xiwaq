module Xiwaq.System.UserManagement.Server.DomainTypes

open Xiwaq.Framework.UserManagement


type UserPasswordHash = UserPasswordHash of string

type UserDomain =
    { User: User
      Email: UserEmail
      PasswordHash: UserPasswordHash }

type UserDomain with
    member this.Id = this.User.Id
    member this.FullName = this.User.FullName

