module Xiwaq.System.UserManagement.Server.Infrastructure.DaoTypes

open System


[<CLIMutable>]
type UserDao =
    { Id: Guid
      Name: string
      Tag: int16
      Email: string
      PasswordHash: string }
