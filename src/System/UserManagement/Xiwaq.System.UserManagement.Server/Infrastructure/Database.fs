module Xiwaq.System.UserManagement.Server.Infrastructure.Database

open LinqToDB
open System.Linq
open Reader


type DatabaseReader<'TDbContext, 'a when 'TDbContext :> IDataContext> = Reader<'TDbContext, 'a>

module Database =

    let getContext<'TDbContext when 'TDbContext :> IDataContext> : Reader<'TDbContext, 'TDbContext> =
        Reader.get


module Query =

    let toResizeArray (q: IQueryable<'a>) : Async<ResizeArray<'a>> = async { return! q.ToListAsync() |> Async.AwaitTask }
    let toArray (q: IQueryable<'a>) = async { return! q.ToArrayAsync() |> Async.AwaitTask }
    let toSeq q = toResizeArray q |> Async.map Seq.cast

    let head (q: IQueryable<'a>) = async { return! q.FirstAsync() |> Async.AwaitTask }
    let tryHead (q: IQueryable<'a>) = async { return! q.Select(Some).FirstOrDefaultAsync() |> Async.AwaitTask }

