module Xiwaq.System.UserManagement.Server.Infrastructure.Repository

open LinqToDB
open Reader
open ReaderAsync

open Xiwaq.Framework.Utils.Randoming
open Xiwaq.Framework.UserManagement
open Xiwaq.Framework.UserManagement.Wrappers
open Xiwaq.Framework.UserManagement.Unwrappers
open Xiwaq.System.UserManagement.Server.DomainTypes
open Xiwaq.System.UserManagement.Server.Infrastructure.DaoTypes
open Xiwaq.System.UserManagement.Server.Infrastructure.DaoMappers
open Xiwaq.System.UserManagement.Server.Infrastructure.Database


type RepositoryEnv =
    { Context: AppDataConnection }

type IHasRepositoryEnv =
    abstract RepositoryEnv: RepositoryEnv

module RepositoryReader =
    let db<'THasRepositoryEnv when 'THasRepositoryEnv :> IHasRepositoryEnv> = reader {
        let! (provider: 'THasRepositoryEnv) = Reader.get
        let env = provider.RepositoryEnv
        return env.Context
    }


module Users =

    let getByEmail (UserEmail email) = reader {
        let! db = RepositoryReader.db
        let q = query {
            for user in db.Users do
            where (user.Email = email)
            take 1
            select user
        }
        let userDao = q |> Query.tryHead
        return userDao |> (UserDomain.ofDao |> Option.map |> Async.map)
    }


    type UniqueUserFullName = private UniqueUserFullName of UserFullName
    let (|UniqueUserFullName|) (UniqueUserFullName x) = x
    module UniqueUserFullName =

        let parse (fullname: UserFullName) = reader {
            let (UserFullName (UserName name, UserTag tag)) = fullname
            let! db = RepositoryReader.db
            let q = query {
                for user in db.Users do
                where (user.Name = name && user.Tag = int16 tag)
                take 1
            }
            return async {
                let! daoOpt = q |> Query.tryHead
                return
                    match daoOpt with
                    | None ->
                        Ok ^ UniqueUserFullName fullname
                    | Some _ ->
                        Error ()
            }
        }


        let generate (name: UserName) : ReaderAsync<_, UniqueUserFullName> = readerAsync {
            let rec loop () : ReaderAsync<_, UniqueUserFullName> = readerAsync {
                let! randTag = Random.nextUInt16 ()
                let randTag = UserTag.Unchecked.create ^ randTag
                match! parse ^ UserFullName.Unchecked.create (name, randTag) with
                | Ok uniqueFullname -> return uniqueFullname
                | Error () ->
                    let! (uniqueFullname: UniqueUserFullName) = loop ()
                    return uniqueFullname
            }
            let! (uniqueFullname: UniqueUserFullName) = loop ()
            return uniqueFullname
        }

module Commands =

    module Users =

        let add (UserId id) (Users.UniqueUserFullName (UserFullName (UserName name, UserTag tag))) (UserEmail email) (UserPasswordHash pwdHash) = reader {
            let! db = RepositoryReader.db
            return async {
                let! c =
                    db.Users
                        .Value((fun x -> x.Id), id)
                        .Value((fun x -> x.Name), name)
                        .Value((fun x -> x.Tag), int16 tag)
                        .Value((fun x -> x.Email), email)
                        .Value((fun x -> x.PasswordHash), pwdHash)
                        .InsertAsync() |> Async.AwaitTask
                match c with
                | 1 -> return Ok ()
                | _ -> return Error ()
            }
        }
