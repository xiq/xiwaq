namespace Xiwaq.System.UserManagement.Server.Infrastructure

open LinqToDB
open LinqToDB.Configuration
open LinqToDB.Data
open LinqToDB.Mapping

open Xiwaq.System.UserManagement.Server.Infrastructure.DaoTypes


type AppDataConnection(options: LinqToDbConnectionOptions<AppDataConnection>) as this =

    inherit DataConnection(options)

    let mapping (schema: MappingSchema) =
        schema.GetFluentMappingBuilder()
            .Entity<UserDao>()
            .HasTableName("Users")
            .HasPrimaryKey(fun x -> x.Id)
        |> ignore

    do mapping this.MappingSchema

    member this.Users = this.GetTable<UserDao>()


module Extensions =

    open Microsoft.Extensions.DependencyInjection
    open LinqToDB.AspNet
    open LinqToDB.AspNet.Logging

    type IServiceCollection with
        member services.AddDataConnection(connectionString) =
            services.AddLinqToDbContext<AppDataConnection>(fun provider options ->
                options
                    .UsePostgreSQL(connectionString)
                    .UseDefaultLogging(provider)
                |> ignore
            )
