module Xiwaq.System.UserManagement.Server.Library

open System
open System.Data
open Microsoft.Extensions.Options
open BCrypt.Net
open Reader
open ReaderAsync

open Xiwaq.Framework.UserManagement
open Xiwaq.Framework.UserManagement.Unwrappers
open Xiwaq.Framework.UserManagement.Wrappers
open Xiwaq.System.UserManagement.Server.Domain
open Xiwaq.System.UserManagement.Server.App
open Xiwaq.System.UserManagement.Server.DomainTypes
open Xiwaq.System.UserManagement.Server.Authentication
open Xiwaq.System.UserManagement.Server.Infrastructure.Repository


module AuthenticationService =

    open Xiwaq.Framework.Utils.Randoming

    let signUp (name: UserName) (email: UserEmail) (password: UserPassword) : ReaderAsync<_, Result<_, _>> =
        readerAsync {
            let! existingUserOpt = Users.getByEmail email
            match existingUserOpt with
            | Some _ -> return Error ^ SignUpError.EmailIsAlreadyTaken
            | None ->
                let! ufullname = Users.UniqueUserFullName.generate name
                let! userId = Random.nextGuid () <!> (UserId.Unchecked.create)
                let pwdHash = UserPassword.hash password
                let! r = Commands.Users.add userId ufullname email pwdHash
                match r with
                | Ok () -> return Ok ()
                | Error () -> return Error ^ SignUpError.Other "User is already exists (WTF)"
        }

    let signIn (email: UserEmail) (UserPassword password) =
        readerAsync {
            let! user = Users.getByEmail email
            match user with
            | Some user ->
                let passwordHash = user.PasswordHash |> fun (UserPasswordHash x) -> x
                let valid = BCrypt.Verify(password, passwordHash)
                match valid with
                | true ->
                    return! reader {
                        let! options = AppReader.resolve<IOptions<AuthenticationOptions>> <!> fun x -> x.Value
                        let now = DateTime.Now
                        let token = Token.generate options now user.Id
                        return Ok { Token = token
                                    User = user.User }
                    }
                | _ -> return Error SignInError.InvalidData
            | None ->
                return Error SignInError.InvalidData
        }

    let getMe (inputToken: InputToken) : ReaderAsync<_, Result<Me, GetMeError>> = readerAsync {

        return notImplemented ""
    }


type AuthenticationService(env: AppEnv) =
    interface IAuthenticationService with
        member _.SignUp(data) = AuthenticationService.signUp data.Name data.Email data.Password |> Reader.run <| env
        member _.SignIn(data) = AuthenticationService.signIn data.Email data.Password |> Reader.run <| env
        member _.GetMe(inputToken) = AuthenticationService.getMe inputToken |> Reader.run <| env


module UserManager =

    let getUserById (userId: UserId) = notImplemented ""

    let getUserByFullName (fullname: UserFullName) = notImplemented ""

