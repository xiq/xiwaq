module Xiwaq.System.UserManagement.Server.Program

open Microsoft.AspNetCore
open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.Configuration


type Startup(config: IConfiguration) =
    member _.Configure(app) = Startup.configureApp config app
    member _.ConfigureServices(services) = Startup.configureServices config services


[<CompiledName "CreateHostBuilder">]
let createHostBuilder(args: string[]) : IWebHostBuilder =
    WebHost.CreateDefaultBuilder(args)
        .ConfigureLogging(Startup.configureLogging)
        .UseStartup<Startup>()


[<EntryPoint>]
let main args =
    (createHostBuilder args).Build().Run()
    0
