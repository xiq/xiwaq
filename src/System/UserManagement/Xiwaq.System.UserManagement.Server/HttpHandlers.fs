module Xiwaq.System.UserManagement.Server.HttpHandlers

open Giraffe
open FSharp.Control.Tasks.V2
open Thoth.Json.Net

open Xiwaq.Framework.UserManagement
open Xiwaq.Framework.UserManagement.Api
open Xiwaq.Framework.UserManagement.Api.Mappers
open Xiwaq.System.UserManagement.Server.Infrastructure


let signUp: HttpHandler =
    fun next ctx -> task {
        let! req = ctx.BindModelAsync<SignUp.Request>()
        let authService = ctx.GetService<IAuthenticationService>()
        let! r =
            asyncResult {
                let! email = UserEmail.parse req.Email |> Result.mapError (fun err -> SignUpError.Other ^ sprintf "Incorrect email (%A)" err)
                let! name = UserName.parse req.Name |> Result.mapError (fun err -> SignUpError.Other ^ sprintf "Incorrect name (%A)" err)
                let! password = UserPassword.parse req.Password |> Result.mapError (fun err -> SignUpError.Other ^ sprintf "Incorrect password (%A)" err)
                let x = authService.SignUp({ Name = name; Email = email; Password = password })
                return! x
            }
        let resp =
            match r with
            | Ok () ->
                { SignUp.Response.Success = true
                  SignUp.Response.Error = null }
            | Error err ->
                { SignUp.Response.Success = false
                  SignUp.Response.Error = sprintf "%A" err }

        return! negotiate resp next ctx
    }

let signIn: HttpHandler =
    fun next ctx -> task {
        let! req = ctx.BindModelAsync<SignIn.Request>()
        let authService = ctx.GetService<IAuthenticationService>()
        let! r =
            asyncResult {
                let! email = UserEmail.parse req.Email |> Result.ignoreError
                let! password = UserPassword.parse req.Password |> Result.ignoreError
                let r = authService.SignIn({ Email = email; Password = password })
                return! r |> AsyncResult.mapError ignore
            }
        let resp =
            match r with
            | Ok me ->
                { SignIn.Response.Success = true
                  SignIn.Response.Me = Me.toDto me }
            | Error _ ->
                { SignIn.Response.Success = false
                  SignIn.Response.Me = Unchecked.defaultof<_> }

        return! negotiate resp next ctx
    }

let getMe: HttpHandler =
    fun next ctx -> task {
        let authService = ctx.GetService<IAuthenticationService>()
        let! req = ctx.BindModelAsync<GetMe.Request>()
        let! r =
            let inputToken = InputToken req.InputToken
            authService.GetMe(inputToken)
        match r with
        | Ok me ->
            let resp =
                { GetMe.Response.Success = true
                  GetMe.Response.Me = me |> Me.toDto }
            return! negotiate resp next ctx
        | Error err ->
            let resp =
                { GetMe.Response.Success = false
                  GetMe.Response.Me = Unchecked.defaultof<_> }
            let resp = {| resp with Error = sprintf "%A" err |}
            return! negotiate resp next ctx
    }


let test: HttpHandler =
    fun next ctx -> task {
        let db = ctx.GetService<AppDataConnection>()
        let names = db.Users |> Seq.map (fun x -> x.Name)
        return! ctx.WriteStringAsync(String.join ", " names)
    }

let webapp: HttpHandler =
    choose [
        route SignUp.route >=> signUp
        route SignIn.route >=> signIn
        route GetMe.route >=> getMe
        test
        setStatusCode 404 >=> text "Not Found"
    ]
