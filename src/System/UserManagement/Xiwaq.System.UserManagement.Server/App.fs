module Xiwaq.System.UserManagement.Server.App

open System
open Microsoft.Extensions.DependencyInjection

open Reader
open Xiwaq.Framework.Utils.Randoming
open Xiwaq.System.UserManagement.Server.Infrastructure.Repository


type AppEnv =
    { ServiceProvider: IServiceProvider
      RepositoryEnv: RepositoryEnv
      Random: IHasRandom }
    interface IHasRandom with member this.Random = this.Random.Random
    interface IHasRepositoryEnv with member this.RepositoryEnv = this.RepositoryEnv

type AppReader<'a> = Reader<AppEnv, 'a>

module AppReader =

    open Xiwaq.System.UserManagement.Server.Infrastructure

    let resolve<'T> = reader {
        let! env = Reader.get
        return env.ServiceProvider.GetRequiredService<'T>()
    }

    let factory (sp: IServiceProvider) =
        { ServiceProvider = sp
          RepositoryEnv = { Context = sp.GetRequiredService<AppDataConnection>() }
          Random = RandomThreadSafe() }
