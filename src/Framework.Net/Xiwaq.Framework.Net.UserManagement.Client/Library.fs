﻿namespace Xiwaq.Framework.UserManagement.Client

open System
open System.Text
open System.Net.Http
open System.Net.Mime
open FSharp.Json

open Xiwaq.Framework.UserManagement
open Xiwaq.Framework.UserManagement.Wrappers
open Xiwaq.Framework.UserManagement.Unwrappers


module private Helpers =

    let request<'TResponse> (client: HttpClient) (reqData: obj) (uri: string) =
        async {
            use reqContent = new StringContent(Json.serialize reqData, Encoding.UTF8, MediaTypeNames.Application.Json)
            let! resp = client.PostAsync(uri, reqContent) |> Async.AwaitTask
            let! r = resp.Content.ReadAsStringAsync() |> Async.AwaitTask
            return Json.deserialize<'TResponse> r
        }


module HttpClientAuthenticationService =

    open Xiwaq.Framework.UserManagement.Api.AuthenticationService

    let signUp (client: HttpClient) (name: UserName) (email: UserEmail) (password: UserPassword) : AsyncResult<unit, SignUpError> =
        async {
            let req =
                { SignUp.Name = name |> fun (UserName x) -> x
                  SignUp.Email = email |> fun (UserEmail x) -> x
                  SignUp.Password = password |> fun (UserPassword x) -> x }
            let! resp = Helpers.request<SignUp.Response> client req SignUp.route
            match resp.Success with
            | true -> return Ok ()
            | false -> return Error SignUpError.EmailIsAlreadyTaken
        }

    let signIn (client: HttpClient) (email: UserEmail) (password: UserPassword) : AsyncResult<Me, SignInError> =
        async {
            let req =
                { SignIn.Request.Email = email |> fun (UserEmail x) -> x
                  SignIn.Request.Password = password |> fun (UserPassword x) -> x }
            let! resp = Helpers.request<SignIn.Response> client req SignIn.route
            match resp.Success with
            | true ->
                return Ok
                    { Me.Token = Token.Unchecked.create resp.Me.Token
                      Me.User =
                          { User.Id = UserId.Unchecked.create resp.Me.User.Id
                            User.FullName = UserFullName.Unchecked.create (UserName.Unchecked.create resp.Me.User.Name, UserTag.Unchecked.create resp.Me.User.Tag) }}
            | false ->
                return Error SignInError.InvalidData
        }

    let getMe (client: HttpClient) (inputToken: InputToken) : AsyncResult<Me, GetMeError> =
        notImplemented ""


type HttpClientAuthenticationService(baseAddress, httpClientFactory: IHttpClientFactory) =

    let httpClient = httpClientFactory.CreateClient(BaseAddress = baseAddress)

    interface IDisposable with
        member this.Dispose() = httpClient.Dispose()

    interface IAuthenticationService with
        member _.SignUp(data) = HttpClientAuthenticationService.signUp httpClient data.Name data.Email data.Password
        member _.SignIn(data) = HttpClientAuthenticationService.signIn httpClient data.Email data.Password
        member _.GetMe(inputToken) = HttpClientAuthenticationService.getMe httpClient inputToken
