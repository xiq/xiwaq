module Xiwaq.Framework.Utils.Base58

type DecodeError =
    | Overflow
    | InvalidBlockSize
    | InvalidSymbol

exception DecodeException of DecodeError

val encode: byte seq -> char seq

val decode: char seq -> byte seq

val decodeFold:
       char seq
    -> onNext: ('s -> byte -> 's)
    -> onError: ('s -> DecodeError -> 'r)
    -> onComplete: ('s -> 'r)
    -> init: 's
    -> 'r
