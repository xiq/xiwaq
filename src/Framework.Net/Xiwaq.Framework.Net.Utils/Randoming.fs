module Xiwaq.Framework.Utils.Randoming

open System
open System.Threading

open Reader


type IHasRandom =
    abstract Random: Random

type RandomThreadSafe() =
    static member private RandomThreadLocal = new ThreadLocal<Random>(fun () -> Random())
    interface IHasRandom with
        member this.Random = RandomThreadSafe.RandomThreadLocal.Value

module Random =

    let private getRandom () = Reader.get<#IHasRandom> |> Reader.map (fun x -> x.Random)

    let next () =
        fun (r: #IHasRandom) -> r.Random.Next()
        |> Reader

    let nextUInt16 () = next () <!> uint16

    let nextGuid () : Reader<#IHasRandom, Guid> = reader {
        return Guid.NewGuid()
    }

    let nextBytes count = reader {
        let! r = getRandom ()
        let buffer = Array.zeroCreate count
        r.NextBytes(buffer)
        return buffer
    }
