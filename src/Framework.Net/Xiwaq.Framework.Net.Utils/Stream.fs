module Xiwaq.Framework.Utils.Stream

open System.IO

let private bytesUnfold =
    let generator (s: Stream) =
        let b = s.ReadByte()
        if b <> -1
        then (byte b, s) |> Some
        else None
    Seq.unfold generator

let private bytesLoop (s: Stream) =
    let rec loop () = seq {
        match s.ReadByte() with
        | -1 -> ()
        | b ->
            yield byte b
            yield! loop ()
    }
    loop ()

let private bytesMutLoop (s: Stream) =
    seq {
        let mutable b = s.ReadByte()
        while b <> -1 do
            yield byte b
            b <- s.ReadByte()
    }

[<Impure>]
let bytes s = bytesMutLoop s
