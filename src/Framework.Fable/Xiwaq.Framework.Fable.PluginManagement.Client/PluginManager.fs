namespace Xiwaq.Framework.PluginManagement.Client

open System
open Xiwaq.Framework.UserManagement
open Xiwaq.Framework.PluginManagement
open Xiwaq.Framework.PluginManagement.Api


module FetchPluginManager =

    open Fable.Core
    open Fetch.Types
    open Thoth.Fetch
    open Thoth.Json
    open Xiwaq.Framework.PluginManagement.Api.PluginManager
    open Xiwaq.Framework.PluginManagement.Wrappers
    open Xiwaq.Framework.PluginManagement.Unwrappers
    open Xiwaq.Framework.UserManagement.Wrappers

    let getInfoById (host: Uri) token (PluginId pid) : Async<PluginInfo> =
        promise {
            let req = { GetInfoById.Request.PluginId = pid }
            let! (resp: GetInfoById.Response) =
                Fetch.post(
                    (Uri(host, GetInfoById.route) |> string),
                    data = {| pluginId = req.PluginId |}
                    , headers = [

                    ]
                    , caseStrategy = CamelCase
                )
            let dto = resp.PluginInfo
            let pluginInfo: PluginInfo =
                { Id = PluginId.Unchecked.create dto.Id
                  Name = PluginName.Unchecked.create dto.Name
                  AuthorId = UserId.Unchecked.create dto.AuthorId }
            return pluginInfo
        } |> Async.AwaitPromise

    let getAllPluginInfos () =
        notImplemented ""


type FetchPluginManager(tokenProvider: ITokenProvider, host) =
    interface IPluginManager with
        member this.GetInfoById(pluginId) = FetchPluginManager.getInfoById host tokenProvider.Token pluginId
        member this.GetAllPluginInfos() = FetchPluginManager.getAllPluginInfos ()
