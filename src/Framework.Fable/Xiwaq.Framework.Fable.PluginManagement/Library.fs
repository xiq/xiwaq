﻿namespace Xiwaq.Framework.PluginManagement

open Reader
open Elmish
open FSharp.Control
open Xiwaq.Framework.PluginManagement


type DynamicState = DynamicState of obj
type DynamicMsg = DynamicMsg of obj

type PluginDynamicState = PluginId * DynamicState
type PluginDynamicMsg = PluginId * DynamicMsg


type PluginInterface<'state, 'msg> =
    { Info: PluginInfo }

type IPluginBridge =
    abstract Listen: target: PluginInterface<'state, 'msg> -> dispatch: ('msg -> unit) -> Async<unit>
    abstract Affect: target: PluginInterface<'state, 'msg> -> msg: ('msg) -> Async<unit>

type IPluginInterfaceProvider =
    abstract GetByName: string -> Async<PluginInterface<'state, 'msg>>


type InitArgs =
    { PluginInfo: PluginInfo
      Bridge: IPluginBridge
      InterfaceProvider: IPluginInterfaceProvider }

type PluginProgram<'state, 'msg> =
    { init: InitArgs -> 'state * Cmd<'msg>
      update: 'msg -> 'state -> 'state * Cmd<'msg>
      view: 'state -> ('msg -> unit) -> Fable.React.ReactElement }


type DynamicPluginProgram = PluginProgram<DynamicState, DynamicMsg>

type Plugin =
    { Info: PluginInfo
      Program: DynamicPluginProgram }


type IPluginLoader =
    abstract Load: PluginId -> Async<Plugin>


module Internal =

    module GlobalPluginProgramLoadedCallback =

        open Fable.Core

        [<Emit "globalThis.XiwaqGlobalPluginProgramLoadedCallback">]
        let mutable private callback: (DynamicPluginProgram -> unit) option = jsNative

        let push dynamicProgram =
            match callback with
            | Some callback -> callback dynamicProgram
            | None -> invalidOp "Plugin program cannot be loaded until the callback is set."

        /// In the `loader` callback a `push` must be called by someone
        let using (loader: unit -> Async<unit>) : Async<DynamicPluginProgram> = async {
            let mutable program = None
            if callback.IsSome then
                JS.console.warn(
                    "Global plugin-program-loaded callback has been overwritten. " +
                    "This might be due to asynchronous plugin loading, and loading errors can occur because of this.")
            callback <- Some ^fun x -> program <- Some x
            do! loader ()
            callback <- None
            match program with
            | None -> return invalidOp "push wasn't called in loader"
            | Some program -> return program
        }


[<RequireQualifiedAccess>]
module PluginProgram =

    open Fable.Core.JsInterop

    let mkPlugin init update view =
        let program: PluginProgram<'state, 'msg> = { init = init; update = update; view = view }
        program

    let run (program: PluginProgram<'state, 'msg>) =
        let dProgram: DynamicPluginProgram = !!program
        Internal.GlobalPluginProgramLoadedCallback.push dProgram
