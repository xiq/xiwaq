module Xiwaq.Framework.PluginManagement.EventCommandExt

open Elmish


type Msg<'command, 'event> =
    | Command of 'command
    | Event of 'event

let update
        (update': 'event -> 'state -> 'state)
        (updateEvent: 'command -> 'state -> 'event * Cmd<'event>)
        msg state
        : 'state * Cmd<Msg<'command, 'event>> =
    match msg with
    | Command command ->
        let event, eventCmd = updateEvent command state
        state, Cmd.batch [ Cmd.ofMsg (Event event); Cmd.map Event eventCmd ]
    | Event event ->
        let state = update' event state
        state, Cmd.none
