﻿[<Fable.Core.Import("*", "@microsoft/signalr")>]
module rec Fable.Import.SignalR

open System
open Fable.Core
open Fable.Core.JS
open Fable.Core.JsInterop


/// Indicates the severity of a log message. Log Levels are ordered in increasing severity. So `Debug` is more severe than `Trace`, etc.
type LogLevel =
    /// Log level for very low severity diagnostic messages.
    | Trace = 0
    /// Log level for low severity diagnostic messages.
    | Debug = 1
    /// Log level for informational diagnostic messages.
    | Information = 2
    /// Log level for diagnostic messages that indicate a non-fatal problem.
    | Warning = 3
    /// Log level for diagnostic messages that indicate a failure in the current operation.
    | Error = 4
    /// Log level for diagnostic messages that indicate a failure that will terminate the entire application.
    | Critical = 5
    /// The highest possible log level. Used when configuring logging to indicate that no log messages should be emitted.
    | None = 6


/// An abstraction that provides a sink for diagnostic messages.
type ILogger =
    /// Called by the framework to emit a diagnostic message.
    /// function log(logLevel: LogLevel, message: string)
    abstract log: logLevel: LogLevel * message: string -> unit


type RetryContext =
    /// elapsedMilliseconds: number
    abstract elapsedMilliseconds: float
    /// previousRetryCount: number
    abstract previousRetryCount: int
    /// retryReason: Error
    abstract retryReason: exn


/// An abstraction that controls when the client attempts to reconnect and how many times it does so.
type IRetryPolicy =
    /// Called after the transport loses the connection.
    /// function nextRetryDelayInMilliseconds(retryContext: RetryContext): number | null
    abstract nextRetryDelayInMilliseconds: retryContext: RetryContext -> float option


/// A protocol abstraction for communicating with SignalR Hubs.
type IHubProtocol =
    /// The name of the protocol. This is used by SignalR to resolve the protocol between the client and server.
    /// name: string
    abstract name: string
    /// The `TransferFormat` of the protocol.
    /// transferFormat: TransferFormat
    abstract transferFormat: TransferFormat
    /// The version of the protocol.
    /// version: number
    abstract version: float
    /// Creates an array of `HubMessage` objects from the specified serialized representation. If `transferFormat` is 'Text', the `input` parameter must be a string, otherwise it must be an ArrayBuffer.
    /// function parseMessages(input: string | ArrayBuffer | Buffer, logger: ILogger): HubMessage[]
//    abstract parseMessages: input: U3<string, ArrayBuffer, Buffer> * logger: ILogger -> HubMessage[]
    /// Writes the specified `HubMessage` to a string or ArrayBuffer and returns it. If `transferFormat` is 'Text', the result of this method will be a string, otherwise it will be an ArrayBuffer.
    /// function writeMessage(message: HubMessage): string | ArrayBuffer
//    abstract writeMessage: message: HubMessage -> U2<string, ArrayBuffer>

type IStreamSubscriber<'T> =
    abstract closed: bool option
    abstract complete: unit -> unit
    abstract error: obj -> unit
    abstract next: 'T -> unit

type ISubscription<'T> =
    abstract dispose: unit -> unit

type IStreamResult<'T> =
    abstract subscribe: subscriber: IStreamSubscriber<'T> -> ISubscription<'T>




// ----
// HubConnection.ts
// ----


/// Describes the current state of the <xref:HubConnection> to the server.
[<StringEnum; RequireQualifiedAccess>]
type HubConnectionState =
    /// The hub connection is connected.
    | Connected
    /// The hub connection is connecting.
    | Connecting
    /// The hub connection is disconnected.
    | Disconnected
    /// The hub connection is disconnecting.
    | Disconnecting
    /// The hub connection is reconnecting.
    | Reconnecting


/// Represents a connection to a SignalR Hub.
type HubConnection =

    /// The server timeout in milliseconds.
    ///
    /// If this timeout elapses without receiving any messages from the server, the connection will be terminated with an error.
    /// The default timeout value is 30,000 milliseconds (30 seconds).
    //TS// serverTimeoutInMilliseconds: number
    abstract serverTimeoutInMilliseconds: float with get, set

    /// Default interval at which to ping the server.
    ///
    /// The default value is 15,000 milliseconds (15 seconds).
    /// Allows the server to detect hard disconnects (like when a client unplugs their computer).
    //TS// keepAliveIntervalInMilliseconds: number
    abstract keepAliveIntervalInMilliseconds: float with get, set

    /// Indicates the state of the {@link HubConnection} to the server.
    //TS// state: HubConnectionState
    abstract state: HubConnectionState with get

    /// Represents the connection id of the {@link HubConnection} on the server. The connection id will be null when the connection is either
    /// in the disconnected state or if the negotiation step was skipped.
    //TS// connectionId: string | null
    abstract connectionId: string option with get

    /// get:
    /// Indicates the url of the {@link HubConnection} to the server.
    ///
    /// set:
    /// Sets a new url for the HubConnection. Note that the url can only be changed when the connection is in either the Disconnected or
    /// Reconnecting states.
    //TS// baseUrl: string
    abstract baseUrl: string with get, set

    /// Starts the connection.
    //TS// function start(): Promise<void>
    abstract start: unit -> Promise<unit>

    /// Stops the connection.
    //TS// stop(): Promise<void>
    abstract stop: unit -> Promise<unit>

    /// Invokes a streaming hub method on the server using the specified name and arguments.
    //TS// stream<T = any>(methodName: string, ...args: any[]): IStreamResult<T>
    abstract stream: methodName: string * [<ParamArray>] args: obj[] -> IStreamResult<'T>

    /// Invokes a hub method on the server using the specified name and arguments. Does not wait for a response from the receiver.
    ///
    /// The Promise returned by this method resolves when the client has sent the invocation to the server. The server may still
    /// be processing the invocation.
    //TS// send(methodName: string, ...args: any[]): Promise<void>
    abstract send: methodName: string * [<ParamArray>] args: obj[] -> Promise<unit>

    /// Invokes a hub method on the server using the specified name and arguments.
    ///
    /// The Promise returned by this method resolves when the server indicates it has finished invoking the method. When the promise
    /// resolves, the server has finished invoking the method. If the server method returns a result, it is produced as the result of
    /// resolving the Promise.
    //TS// invoke<T = any>(methodName: string, ...args: any[]): Promise<T>
    abstract invoke: methodName: string * [<ParamArray>] args: obj[] -> Promise<'T>

    /// Registers a handler that will be invoked when the hub method with the specified method name is invoked.
    //TS// on(methodName: string, newMethod: (...args: any[]) => void)
    abstract on: methodName: string * newMethod: (obj[] -> unit) -> unit

    /// Removes all handlers for the specified hub method.
    //TS// off(methodName: string): void
    abstract off: methodName: string -> unit

    /// Removes the specified handler for the specified hub method.
    ///
    /// You must pass the exact same Function instance as was previously passed to {@link @microsoft/signalr.HubConnection.on}. Passing a different instance (even if the function
    /// body is the same) will not remove the handler.
    //TS// off(methodName: string, method: (...args: any[]) => void): void
    abstract off: methodName: string * method: (obj[] -> unit) -> unit

    //TS// off(methodName: string, method?: (...args: any[]) => void): void
    abstract off: methodName: string * ?method: (obj[] -> unit) -> unit

    /// Registers a handler that will be invoked when the connection is closed.
    //TS// onclose(callback: (error?: Error) => void)
    abstract onclose: callback: (exn -> unit) -> unit

    /// Registers a handler that will be invoked when the connection starts reconnecting.
    //TS// onreconnecting(callback: (error?: Error) => void)
    abstract onreconnecting: callback: (exn -> unit) -> unit

    /// Registers a handler that will be invoked when the connection successfully reconnects.
    //TS// onreconnected(callback: (connectionId?: string) => void)
    abstract onreconnected: callback: (string -> unit) -> unit




// ----
// IHttpConnectionOptions.ts
// ----


/// Options provided to the 'withUrl' method on {@link @microsoft/signalr.HubConnectionBuilder} to configure options for the HTTP-based transports.
type IHttpConnectionOptions =

    /// An {@link @microsoft/signalr.HttpClient} that will be used to make HTTP requests.
    //TS// httpClient?: HttpClient
    abstract httpClient: HttpClient option with get, set

    /// An {@link @microsoft/signalr.HttpTransportType} value specifying the transport to use for the connection.
    //TS// transport?: HttpTransportType | ITransport
    abstract transport: U2<HttpTransportType, ITransport> option with get, set

    /// Configures the logger used for logging.
    ///
    /// Provide an {@link @microsoft/signalr.ILogger} instance, and log messages will be logged via that instance. Alternatively, provide a value from
    /// the {@link @microsoft/signalr.LogLevel} enumeration and a default logger which logs to the Console will be configured to log messages of the specified
    /// level (or higher).
    //TS// logger?: ILogger | LogLevel
    abstract logger: U2<ILogger, LogLevel> option with get, set

    /// A function that provides an access token required for HTTP Bearer authentication.
    //TS// accessTokenFactory?(): string | Promise<string>
    abstract accessTokenFactory: unit -> U2<string, Promise<string>>

    /// A boolean indicating if message content should be logged.
    ///
    /// Message content can contain sensitive user data, so this is disabled by default.
    //TS// logMessageContent?: boolean
    abstract logMessageContent: bool option with get, set

    /// A boolean indicating if negotiation should be skipped.
    ///
    /// Negotiation can only be skipped when the {@link @microsoft/signalr.IHttpConnectionOptions.transport} property is set to 'HttpTransportType.WebSockets'.
    //TS// skipNegotiation?: boolean
    abstract skipNegotiation: bool option with get, set




// ----
// HubConnectionBuilder.ts
// ----


/// A builder for configuring {@link @microsoft/signalr.HubConnection} instances.
type HubConnectionBuilder =

    /// Configures console logging for the {@link @microsoft/signalr.HubConnection}.
    //TS// configureLogging(logLevel: LogLevel): HubConnectionBuilder
    abstract configureLogging: logLevel: LogLevel -> HubConnectionBuilder

    /// Configures custom logging for the {@link @microsoft/signalr.HubConnection}.
    //TS// configureLogging(logger: ILogger): HubConnectionBuilder
    abstract configureLogging: logger: ILogger -> HubConnectionBuilder

    /// Configures custom logging for the {@link @microsoft/signalr.HubConnection}.
    //TS// configureLogging(logLevel: string): HubConnectionBuilder
    abstract configureLogging: logLevel: string -> HubConnectionBuilder

    /// Configures custom logging for the {@link @microsoft/signalr.HubConnection}.
    //TS// configureLogging(logging: LogLevel | string | ILogger): HubConnectionBuilder
    abstract configureLogging: logging: U3<LogLevel, string, ILogger> -> HubConnectionBuilder

    /// Configures the {@link @microsoft/signalr.HubConnection} to use HTTP-based transports to connect to the specified URL.
    //TS// withUrl(url: string): HubConnectionBuilder
    abstract withUrl: url: string -> HubConnectionBuilder

    /// Configures the {@link @microsoft/signalr.HubConnection} to use the specified HTTP-based transport to connect to the specified URL.
    //TS// withUrl(url: string, transportType: HttpTransportType): HubConnectionBuilder
    abstract withUrl: url: string * transportType: HttpTransportType -> HubConnectionBuilder

    /// Configures the {@link @microsoft/signalr.HubConnection} to use HTTP-based transports to connect to the specified URL.
    //TS// withUrl(url: string, options: IHttpConnectionOptions): HubConnectionBuilder
    abstract withUrl: url: string * options: IHttpConnectionOptions -> HubConnectionBuilder
    //TS// withUrl(url: string, transportTypeOrOptions?: IHttpConnectionOptions | HttpTransportType): HubConnectionBuilder
    abstract withUrl: url: string * ?transportTypeOrOptions: U2<IHttpConnectionOptions, HttpTransportType> -> HubConnectionBuilder

    /// Configures the {@link @microsoft/signalr.HubConnection} to use the specified Hub Protocol.
    //TS// withHubProtocol(protocol: IHubProtocol): HubConnectionBuilder
    abstract withHubProtocol: protocol: IHubProtocol -> HubConnectionBuilder

    /// Configures the {@link @microsoft/signalr.HubConnection} to automatically attempt to reconnect if the connection is lost.
    /// By default, the client will wait 0, 2, 10 and 30 seconds respectively before trying up to 4 reconnect attempts.
    //TS// withAutomaticReconnect(): HubConnectionBuilder
    abstract withAutomaticReconnect: unit -> HubConnectionBuilder

    /// Configures the {@link @microsoft/signalr.HubConnection} to automatically attempt to reconnect if the connection is lost.
    //TS// withAutomaticReconnect(retryDelays: number[]): HubConnectionBuilder
    abstract withAutomaticReconnect: retryDelays: float[] -> HubConnection

    /// Configures the {@link @microsoft/signalr.HubConnection} to automatically attempt to reconnect if the connection is lost.
    //TS// withAutomaticReconnect(reconnectPolicy: IRetryPolicy): HubConnectionBuilder
    abstract withAutomaticReconnect: reconnectPolicy: IRetryPolicy -> HubConnectionBuilder

    //TS// withAutomaticReconnect(retryDelaysOrReconnectPolicy?: number[] | IRetryPolicy): HubConnectionBuilder
    abstract withAutomaticReconnect: ?retryDelaysOrReconnectPolicy: U2<float[], IRetryPolicy> -> HubConnectionBuilder

    /// Creates a {@link @microsoft/signalr.HubConnection} from the configuration options specified in this builder.
    //TS// build(): HubConnection
    abstract build: unit -> HubConnection

type HubConnectionBuilderConstructor =
    [<Emit "new $0($1...)">]
    abstract Create: unit -> HubConnectionBuilder

[<RequireQualifiedAccess>]
module Constructors =
    let HubConnectionBuilder: HubConnectionBuilderConstructor = jsNative


/// Specifies a specific HTTP transport type.
type HttpTransportType =
    /// Specifies no transport preference.
    | None = 0
    /// Specifies the WebSockets transport.
    | WebSockets = 1
    /// Specifies the Server-Sent Events transport.
    | ServerSentEvents = 2
    /// Specifies the Long Polling transport.
    | LongPolling = 4


/// Defines the type of a Hub Message.
type MessageType =
    /// Indicates the message is an Invocation message and implements the `InvocationMessage` interface.
    | Invocation = 1
    /// Indicates the message is a StreamItem message and implements the `StreamItemMessage` interface.
    | StreamItem = 2
    /// Indicates the message is a Completion message and implements the `CompletionMessage` interface.
    | Completion = 3
    /// Indicates the message is a Stream Invocation message and implements the `StreamInvocationMessage` interface.
    | StreamInvocation = 4
    /// Indicates the message is a Cancel Invocation message and implements the `CancelInvocationMessage` interface.
    | CancelInvocation = 5
    /// Indicates the message is a Ping message and implements the `PingMessage` interface.
    | Ping = 6
    /// Indicates the message is a Close message and implements the `CloseMessage` interface.
    | Close = 7


/// Specifies the transfer format for a connection.
type TransferFormat =
    /// Specifies that only text data will be transmitted over the connection.
    | Text = 1
    /// Specifies that binary data will be transmitted over the connection.
    | Binary = 2


type ITransport =
    /// onclose: (error?: Error) => void | null
    abstract onclose: (exn option -> unit option)
    /// onreceive: (data: string | ArrayBuffer) => void | null
    abstract onreceive: (U2<string, ArrayBuffer> -> unit option)
    /// function connect(url: string, transferFormat: TransferFormat): Promise<void>
    abstract connect: url: string * transferFormat: TransferFormat -> Promise<unit>
    /// function send(data: any): Promise<void>
    abstract send: data: obj -> Promise<unit>
    /// function stop(): Promise<void>
    abstract stop: unit -> Promise<unit>


type HttpResponse =
//    new(statusCode: int) = jsNative<HttpResponse>
    abstract content: U2<string, ArrayBuffer> option
    abstract statusCode: int

module HttpResponse =
    let Create (statusCode: int) : HttpResponse = jsNative


type HttpClient =
    abstract delete: url: string -> Promise<HttpResponse>
//    abstract delete:

type HttpError =
    [<Emit "new $0($1, $2)">]
    abstract Create: errorMessage: string * statusCode: int
