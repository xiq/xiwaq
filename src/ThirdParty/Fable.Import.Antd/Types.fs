namespace Fable.Import.Antd

open Fable.Core


[<StringEnum>]
type Breakpoint =
    | Xs | Sm | Md | Lg | Xl | Xxl

[<StringEnum>]
type Theme =
    | Light | Dark

[<StringEnum>]
type MenuMode =
    | Vertical | Horizontal | Inline

type TreeData =
    { Key: string
      Title: string
      Children: TreeData seq }


[<StringEnum>]
type SpaceAlign =
    | Start | End | Center | Baseline

[<StringEnum>]
type SpaceDirection =
    | Vertical | Horizontal

[<StringEnum>]
type SpaceSize =
    | Small | Middle | Large | Number of int
