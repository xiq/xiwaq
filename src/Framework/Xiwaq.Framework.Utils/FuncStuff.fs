[<AutoOpen>]
module FuncStuff

let inline ( ^ ) f x = f x

let cnst x = fun _ -> x
let konst = cnst

let delay x = fun () -> x

let rec nest n f x = if n = 0 then x else nest (n - 1) f (f x)


module Tupling =
    let tuple2 x y = x, y
    let tuple3 x y z = x, y, z
    let tuple4 x y z w = x, y, z, w
let tuple = Tupling.tuple2


module Curring =
    let curry2 f x y = f (x, y)
    let curry3 f x y z = f (x, y, z)
    let curry4 f x y z w = f (x, y, z, w)

    let uncurry2 f (x, y) = f x y
    let uncurry3 f (x, y, z) = f x y z
    let uncurry4 f (x, y, z, w) = f x y z w
let curry = Curring.curry2
let uncurry = Curring.uncurry2


module Composing =
    let compose  f g x = g (f x)
    let compose2 f g x = compose  (f x) g
    let compose3 f g x = compose2 (f x) g
    let compose4 f g x = compose3 (f x) g
    let compose5 f g x = compose4 (f x) g

    module Operators =
        let ( >>> ) = compose2
        let ( >>>> ) = compose3
        let ( >>>>> ) = compose4
        let ( >>>>>> ) = compose5


module Flipping =
    let flip'ab f a b = f b a
    let flip'ac f a b c = f c b a
    let flip'bc f a b c = f a c b
    let flip'ad f a b c d = f d b c a
    let flip'bd f a b c d = f a d c b
    let flip'cd f a b c d = f a b d c
    let flip'ae f a b c d e = f e b c d a
    let flip'be f a b c d e = f a e c d b
    let flip'ce f a b c d e = f a b e d c
    let flip'de f a b c d e = f a b c e d
let flip = Flipping.flip'ab


module Swapping =
    let swap'2'ab (a, b) = b, a
    let swap'3'ab (a, b, c) = b, a, c
    let swap'3'ac (a, b, c) = c, b, a
    let swap'3'bc (a, b, c) = a, c, b
    let swap'4'ab (a, b, c, d) = b, a, c, d
    let swap'4'ac (a, b, c, d) = c, b, a, d
    let swap'4'ad (a, b, c, d) = d, b, c, a
    let swap'4'bc (a, b, c, d) = a, c, b, d
    let swap'4'bd (a, b, c, d) = a, d, c, b
    let swap'4'cd (a, b, c, d) = a, b, d, c
let swap = Swapping.swap'2'ab
