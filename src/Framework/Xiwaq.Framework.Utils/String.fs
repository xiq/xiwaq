[<AutoOpen>]
module StringExtensions

[<RequireQualifiedAccess>]
module String =

    open System

    let isEmpty (s: string) = String.IsNullOrWhiteSpace(s)

    let (|IsEmpty|_|) (s: string) =
        if isEmpty s
        then Some IsEmpty
        else None

    let split (sep: char) (s: string) = s.Split(sep) |> Array.toList

    let (|Split|) sep s = split sep s

    let join sep (s: string seq) = String.Join(sep, s)
