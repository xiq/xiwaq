module Reader


[<Struct>]
type Reader<'env, 'a> = Reader of ('env -> 'a)


module Reader =

    let run (Reader f) (env: 'env) = f env
    let run'(env: 'env)  (Reader f) = f env

    let get<'env> : Reader<'env, 'env> = Reader ^fun env -> env

    let retn x : Reader<'env, 'a> = Reader ^fun _ -> x

    let bind f (r: Reader<'env, 'a>) : Reader<'env, 'b> =
        Reader ^fun env -> run (f (run r env)) env

    let map f (x: Reader<'env, 'a>) : Reader<'env, 'b> = bind (f >> retn) x

    let apply f x = bind (fun f' -> map f' x) f

    let withEnv (f: 'envb -> 'enva) (r: Reader<'enva, 'a>) : Reader<'envb, 'a> =
        Reader ^fun (env': 'envb) -> run r (f env')

    let lift1 (f: 'env -> 'a -> 'b) = fun x -> Reader ^fun env -> f env x
    let lift2 (f: 'env -> 'a -> 'b -> 'c) = fun x y -> Reader ^fun env -> f env x y


type Reader<'env, 'a> with
    static member inline ( >>= )(x, f) = Reader.bind f x
    static member inline ( <!> )(x, f) = Reader.map f x
    static member inline ( <*> )(x, f) = Reader.apply f x


type ReaderBuilder() =
    member _.Bind(x, f) = Reader.bind f x
    member _.Return(x) = Reader.retn x
    member _.ReturnFrom(x: Reader<'env, 'a>) = x
    member _.Zero() = Reader.retn ()
    member _.Delay(f) = f ()


let reader = ReaderBuilder()


type Reader<'env, 'a> with
    member this.Run(env) =
        let (Reader f) = this
        f env
