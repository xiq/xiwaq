[<AutoOpen>]
module OptionExtensions

[<RequireQualifiedAccess>]
module Option =

    let apply f x = Option.bind (fun f' -> Option.map f' x) f

    let reverse = function
        | Some () -> None
        | None -> Some ()

    /// Erases Error case value!
    let ofResult = function
        | Ok x -> Some x
        | Error _ -> None

    let toResult = function
        | Some x -> Ok x
        | None -> Error ()

    let ofBool = function
        | true -> Some ()
        | false -> None

    let ofRef = function
        | true, x -> Some x
        | false, _ -> None

    /// Ignores the exception
    let tryWith f =
        try f () |> Some
        with _ -> None

    let getOrExn ex = function
        | Some x -> x
        | None -> raise ex


[<AutoOpen>]
module Builders =
    type OptionBuilder() =
        member _.Bind(x, f) = Option.bind f x
        member _.Return(x) = Some x
        member _.Zero() = None

    let option = OptionBuilder()


#if FSHARP5
#nowarn "1215" // TODO: (плакать) Дождаться F# 5 с поддержкой перегрузки операторов в уже существующих типах.

module OptionOperatorExtensions =
    type Option<'a> with
        static member ( >>= ) (x, f) = Option.bind f x
        static member ( <!> ) (x, f) = Option.map f x
        static member ( <*> ) (x, f) = Option.apply f x

#endif
