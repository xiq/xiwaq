namespace global


type Void internal () = class end

module Void =

    let absurd (_: Void) = failwith "No sense"

    module Internal =

        [<System.Obsolete "oh no, please no">]
        let instance = Void()
