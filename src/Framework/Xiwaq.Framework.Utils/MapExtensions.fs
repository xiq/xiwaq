[<AutoOpen>]
module MapExtensions


[<RequireQualifiedAccess>]
module Map =

    let values x = Map.toSeq >> Seq.map snd <| x
    let keys x = Map.toSeq >> Seq.map fst <| x
