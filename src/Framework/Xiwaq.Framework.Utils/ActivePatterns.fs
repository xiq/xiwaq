[<AutoOpen>]
module ActivePatterns


let (|Equals|_|) x y = if x = y then Some Equals else None

let (|NotEquals|_|) x y = if x <> y then Some NotEquals else None

let inline (|Is|_|) f x = if f x then Some () else None

let f = fun x -> x > 2
match 2 with
| Is f -> 1
| _ -> 0
|> ignore
