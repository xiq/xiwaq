module TypedBoolean

[<RequireQualifiedAccess>]
/// Inclusive Or
type Or<'a, 'b> =
    | Left of 'a
    | Right of 'b
    | Both of 'a * 'b

[<RequireQualifiedAccess>]
/// Alias for Or
type Ior<'a, 'b> = Or<'a, 'b>

[<RequireQualifiedAccess>]
type And<'a, 'b> =
    | Both of 'a * 'b

[<RequireQualifiedAccess>]
/// Exclusive or
type Xor<'a, 'b> =
    | Left of 'a
    | Right of 'b


[<RequireQualifiedAccess>]
module And =

    /// a and b = b and a
    let rev = function
        | And.Both (x, y) -> And.Both (y, x)

    let uncurry f = fun x y -> f (And.Both (x, y))
    let curry f = fun (And.Both (x, y)) -> f x y

    let toTuple (And.Both (x, y)) = (x, y)
    let ofTuple (x, y) = And.Both (x, y)


[<RequireQualifiedAccess>]
module Or =

    /// or = and + xor
    let (|And|Xor|) = function
        | Ior.Both (x, y) -> And (And.Both (x, y))
        | Ior.Left x -> Xor (Xor.Left x)
        | Ior.Right x -> Xor (Xor.Right x)

    /// a or b = b or a
    let rev = function
        | Ior.Both (x, y) -> Ior.Both (y, x)
        | Ior.Left x -> Ior.Right x
        | Ior.Right x -> Ior.Left x

/// Alias for Or
module Ior = Or


[<RequireQualifiedAccess>]
module Xor =

    /// a xor b = b xor a
    let rev = function
        | Xor.Left x -> Xor.Right x
        | Xor.Right x -> Xor.Left x


///// (a or b) and (c or d) = (a or c) or (b or d)
//let foo (x: And<Or<'a, 'b>, Or<'c, 'd>>) : Or<Or<'a, 'c>, Or<'b, 'd>> =
//    let (And.Both (x, y)) = x
//    match x, y with
//    | Or.Both (a, b), Or.Both (c, d) -> Or.Both (Or.Both (a, c), Or.Both (b, d))
//    | Or.Left a, Or.Left c -> Or.Left (Or.Both (a, c))
//    | Or.Right b, Or.Right d -> Or.Right (Or.Both (b, d))
//    | Or.Left a, Or.Right d -> Or.Both (Or.Left a, Or.Right d)
//    | Or.Right b, Or.Left c -> Or.Both (Or.Right c, Or.Left b)
//    | Or.Both (a, b), Or.Left c -> Or.Both (Or.Both (a, c), Or.Left b)
//    | Or.Both (a, b), Or.Right d -> Or.Both (Or.Left a, Or.Both (b, d))
//    | Or.Left a, Or.Both (c, d) -> Or.Both (Or.Both (a, c), Or.Right d)
//    | Or.Right b, Or.Both (c, d) -> Or.Both (Or.Right c, Or.Both (b, d))


[<RequireQualifiedAccess>]
module Result =

    let ofXor = function
        | Xor.Left x -> Ok x
        | Xor.Right x -> Error x

    let toXor = function
        | Ok x -> Xor.Left x
        | Error x -> Xor.Right x

    let andOks (x: Result<'a, 'e>) (y: Result<'b, 'f>) : Result<And<'a, 'b>, Ior<'e, 'f>> =
        match x, y with
        | Ok a, Ok c -> Ok (And.Both (a, c))
        | Error b, Error d -> Error (Ior.Both (b, d))
        | Error b, Ok _ -> Error (Ior.Left b)
        | Ok _, Error d -> Error (Ior.Right d)

    let andErrors (x: Result<'a, 'e>) (y: Result<'b, 'f>) : Result<Ior<'a, 'b>, And<'e, 'f>> =
        match x, y with
        | Error e, Error f -> Error (And.Both (e, f))
        | Ok a, Ok b -> Ok (Ior.Both (a, b))
        | Error _, Ok b -> Ok (Ior.Right b)
        | Ok a, Error _ -> Ok (Ior.Left a)
