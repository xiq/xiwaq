[<AutoOpen>]
module ReaderEnvExtensions

open Reader


type IHasEnv<'env> =
    abstract Env: 'env


module private Example =

    type IAdder =
        abstract Add: int * int -> int

    module Adder =

        let get<'env when 'env :> IHasEnv<IAdder>> =
            Reader.get<'env> <!> fun env -> env.Env

        let add x y =
            reader {
                let! env = get
                return env.Add(x, y)
            }

    type AppEnv() =
        interface IHasEnv<IAdder> with member _.Env = { new IAdder with member _.Add(x, y) = x + y }


    let appenv = AppEnv()

    let r = Adder.add 2 3 |> Reader.run' appenv
