[<AutoOpen>]
module ResultExtensions


[<RequireQualifiedAccess>]
module Result =

    let apply f x = Result.bind (fun f' -> Result.map f' x) f

    let bindError f x =
        match x with
        | Error x -> f x
        | Ok x -> Ok x

    let ofOption = function
        | Some x -> Ok x
        | None -> Error ()

    let toOption = function
        | Ok x -> Some x
        | Error () -> None


    let tryWith f =
        try f () |> Ok
        with ex -> Error ex


    exception ResultIsErrorException

    /// Throws an exception if the value is not Ok.
    let getOk = function
        | Ok x -> x
        | Error _ -> raise ResultIsErrorException

    exception ResultIsOkException

    /// Throws an exception if the value is not Error.
    let getError = function
        | Ok _ -> raise ResultIsOkException
        | Error e -> e


    let tryOk = function
        | Ok x -> Some x
        | Error _ -> None

    let tryError = function
        | Ok _ -> None
        | Error x -> Some x


    let ofOptionOr ifNone = function
        | Some x -> Ok x
        | None -> Error ifNone


    /// Returns Ok case if is Ok, or throws an exception by given mapping from Error case.
    let getOkOrWithExn (exf: 'e -> #exn) : Result<'s, 'e> -> 's = function
        | Ok x -> x
        | Error e -> exf e |> raise

    //let or' r1 r2 =
    //    match r1 with
    //    | Ok x -> Ok x
    //    | _ -> r2
    //
    //let and' r1 r2 =
    //    match r1 with
    //    | Error x -> Error x
    //    | _ -> r2


    let ignoreOk r = Result.map ignore r
    let ignoreError r = Result.mapError ignore r


[<AutoOpen>]
module Builders =

    type ResultBuilder() =
        member _.Bind(x, f) = Result.bind f x
        member _.Return(x) = Ok x
        member _.ReturnFrom(x: Result<_, _>) = x
        member _.TryWith(x, f) =
            try x
            with e -> f e
        member _.Delay(f) = f ()

    let result = ResultBuilder()

    module ResultBuilderOptionExtensions =
        type ResultBuilder with
            member _.Bind(x, f) = Result.bind f (Result.ofOption x)
            member _.ReturnFrom(x: Option<_>) = Result.ofOption x
