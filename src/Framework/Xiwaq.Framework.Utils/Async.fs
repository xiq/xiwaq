namespace global

[<RequireQualifiedAccess>]
module Async =

    let retn = async.Return

    let bind f x = async.Bind(x, f)

    let map f = bind (f >> retn)

    let apply f x = bind (fun f' -> map f' x) f


    let zero = async.Zero()


#if FSHARP5

#nowarn "1215" // TODO: (плакать) Дождаться F# 5 с поддержкой перегрузки операторов в уже существующих типах.
type Async<'a> with
    static member ( >>= ) (x, f) = Async.bind f x
    static member ( <!> ) (x, f) = Async.map f x
    static member ( <*> ) (x, f) = Async.apply f x

#endif
