module ReaderAsync

open System
open Reader

type ReaderAsync<'env, 'a> = Reader<'env, Async<'a>>

[<RequireQualifiedAccess>]
module ReaderAsync =

    let retn x : ReaderAsync<'env, 'a> = Async.retn >> Reader.retn <| x

    let get<'env> : ReaderAsync<'env, 'env> =
        Reader Async.retn

    let bind (f: 'a -> ReaderAsync<'env, 'b>) (x: ReaderAsync<'env, 'a>) : ReaderAsync<'env, 'b> =
        reader {
            let! x' = x
            let! env = Reader.get
            return async {
                let! y = x'
                return! Reader.run (f y) env
            }
        }

    let map f = bind (f >> retn)
    let apply f x : ReaderAsync<'env, 'b> = f |> bind (fun f -> map f x)
    let ofReader (r: Reader<'env, 'a>) : ReaderAsync<'env, 'a> = Reader.map Async.retn r


type ReaderAsyncBuilder() =
    // ReaderAsync
    member _.Bind(x, f) = ReaderAsync.bind f x
    member _.ReturnFrom(x: ReaderAsync<'env, 'a>) = x
    member _.TryWith(x: ReaderAsync<'env, 'a>, f: exn -> ReaderAsync<'env, 'a>) : ReaderAsync<'env, 'a> =
        reader {
            let! x' = x
            let! env = Reader.get
            let f' ex : Async<'a> = f >> flip Reader.run env <| ex
            return async.TryWith(x', f')
        }
    // Reader
    member _.Bind(x, f) = ReaderAsync.bind f (ReaderAsync.ofReader x)
    member _.ReturnFrom(x) = ReaderAsync.ofReader x
    //Async
    member _.ReturnFrom(x): ReaderAsync<_, _> = Reader.retn x

    member _.Return(x) = ReaderAsync.retn x
    member _.Zero() = ReaderAsync.retn ()
    member _.Delay(f) = f ()

let readerAsync = ReaderAsyncBuilder()
