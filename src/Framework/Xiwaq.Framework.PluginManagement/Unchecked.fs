namespace Xiwaq.Framework.PluginManagement

(*
PluginId
PluginName
*)
// ^(\w+)$

(*
    [<RQA>] module $1 = module Unchecked = let create = $1
*)
module Wrappers =
    [<RQA>] module PluginId = module Unchecked = let create = PluginId
    [<RQA>] module PluginName = module Unchecked = let create = PluginName

(*
    let (|$1|) ($1 x) = x
*)
module Unwrappers =
    let (|PluginId|) (PluginId x) = x
    let (|PluginName|) (PluginName x) = x
