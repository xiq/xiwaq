[<AutoOpen>]
module Xiwaq.Framework.PluginManagement.Types

open Xiwaq.Framework.UserManagement


type Id = System.Guid

//[<CustomEquality; NoComparison>]
type PluginId =
    | PluginId of Id
//    override this.GetHashCode() = let (PluginId x) = this in x.GetHashCode()
//    override this.Equals(other) =
//        let (PluginId x) = this
//        match other with
//        | :? PluginId as other ->
//            let (PluginId y) = other
//            x.ToString().Equals(y.ToString())
//        | _ -> false

type PluginName = PluginName of string

type PluginInfo =
    { Id: PluginId
      Name: PluginName
      AuthorId: UserId }
