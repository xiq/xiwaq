﻿namespace Xiwaq.Framework.UserManagement.Api

open System


type 'a nullable = 'a

[<CLIMutable>]
type UserDto =
    { Id: Guid
      Name: string
      Tag: uint16 }

[<CLIMutable>]
type MeDto =
    { User: UserDto
      Token: string }


[<AutoOpen>]
module AuthenticationService =

    [<RequireQualifiedAccess>]
    module SignUp =
        [<CLIMutable>]
        type Request =
            { Name: string
              Email: string
              Password: string }
        [<CLIMutable>]
        type Response =
            { Success: bool
              Error: string nullable }
        let route = "/signup"

    [<RequireQualifiedAccess>]
    module SignIn =
        [<CLIMutable>]
        type Request =
            { Email: string
              Password: string }
        type Response =
            { Success: bool
              Me: MeDto nullable }
        let route = "/signin"

    [<RequireQualifiedAccess>]
    module GetMe =
        type Request =
            { InputToken: string }
        type Response =
            { Success: bool
              Me: MeDto nullable }
        let route = "/getMe"


[<AutoOpen>]
module UserManager =

    [<CLIMutable>]
    type GetByIdRequest =
        { UserId: Guid }

    [<CLIMutable>]
    type GetByIdResponse =
        { User: UserDto nullable }

    [<CLIMutable>]
    type GetByFullNameRequest =
        { UserName: string
          UserTag: uint16 }
