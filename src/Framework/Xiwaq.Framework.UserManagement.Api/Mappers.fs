module Xiwaq.Framework.UserManagement.Api.Mappers

open Xiwaq.Framework.UserManagement
open Xiwaq.Framework.UserManagement.Unwrappers

module User =
    let toDto (user: User) : UserDto =
        let id = let (UserId x) = user.Id in x
        let (UserFullName (UserName name, UserTag tag)) = user.FullName
        { Id = id; Name = name; Tag = tag }
module Me =
    let toDto (me: Me) : MeDto =
        { User = me.User |> User.toDto
          Token = me.Token |> fun (Token x) -> x }

module AuthenticationService =
    module SignInData =
        let toDto (data: SignInData) : SignIn.Request =
            { Email = data.Email |> fun (UserEmail x) -> x
              Password = data.Password |> fun (UserPassword x) -> x }
    module SignUpData =
        let toDto (data: SignUpData) : SignUp.Request =
            { Email = data.Email |> fun (UserEmail x) -> x
              Name = data.Name |> fun (UserName x) -> x
              Password = data.Password |> fun (UserPassword x) -> x }
