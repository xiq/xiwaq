﻿namespace Xiwaq.Framework.PluginManagement.Api

open System


type PluginInfoDto =
    { Id: Guid
      Name: string
      AuthorId: Guid }

module PluginManager =

    /// GET /plugin/{id}/info
    /// POST /plugin/info
    [<RQA>]
    module GetInfoById =
        [<CLIMutable>]
        type Request =
            { PluginId: Guid }
        [<CLIMutable>]
        type Response =
            { PluginInfo: PluginInfoDto }
        let route = "/plugin/info"

module PluginScriptLinkProvider =
    /// GET /plugin/{id}/script
    [<RQA>]
    module GetLink =
        let route = "/plugin/{id}/script"
