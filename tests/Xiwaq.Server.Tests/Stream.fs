module Xiwaq.Server.Tests.Stream

open System.IO
open BenchmarkDotNet.Attributes
open BenchmarkDotNet.Engines
open Expecto
open Expecto.BenchmarkDotNet
open BenchmarkDotNet

open Xiwaq.Server.Utils

type StreamToBytes() =
    let consumer = Consumer()
    let urandomStream = File.OpenRead("/dev/urandom")

    [<Params(10, 100, 1000)>]
    let mutable N = 0

    [<GlobalSetup>]
    member this.Setup() =
        System.Diagnostics.Debugger.Launch()

    [<Benchmark>]
    member this.WithCE() =
        let xs =
            urandomStream
            |> Stream.bytes
            |> Seq.take N
        xs.Consume(consumer)

    [<Benchmark>]
    member this.WithSeqModule() =
        let xs =
            urandomStream
            |> Stream.bytes
            |> Seq.take N
        xs.Consume(consumer)

//[<Tests>]
let tests = testList "Stream" [
    test "StreamToBytes" {
        benchmark<StreamToBytes> benchmarkConfig (fun _ -> null) |> ignore
    }
]
